import React, {Component} from 'react';

class Budgettable extends Component {

    render() {

        const {data, categories} = this.props;

        const List_item_income = data.incomes.map((data, i) => {

            let categoryName;
            for (let j = 0; j < categories.length; j++) {
                if (categories[j].id == data.category) {
                    categoryName = categories[j].name;
                }
            }

            return (
                <div key={i} className="budget-list-item">
                    <div className="item-note">
                        {data.note} ({categoryName})
                    </div>
                    <div className="item-budget">
                        {data.amount} AZN
                    </div>
                    <div className="clearfix"></div>
                </div>
            )
        });

        const List_item_expense = data.expense.map((data, i) => {
            return (
                <div key={i} className="budget-list-item">
                    <div className="item-note">
                        {data.note} (Meiset)
                    </div>
                    <div className="item-budget">
                        {data.amount} AZN
                    </div>
                    <div className="clearfix"></div>
                </div>
            )
        });

        return (
            <div className="budget-table container mt-4">
                <div className="row">
                    <div className="col-sm-6">
                        <h2 className="text-dark">Income</h2>
                        {List_item_income}
                    </div>
                    <div className="col-sm-6">
                        <h2 className="text-danger">Expense</h2>
                        {List_item_expense}
                    </div>
                </div>
            </div>
        )
    }

}

export default Budgettable;
