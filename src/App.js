import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Addform from './Addform';
import Budgettable from './Budgettable';

const categories = [
    {'id': 1, 'name': 'Meiset'},
    {'id': 2, 'name': 'Neqliyyat'},
    {'id': 3, 'name': 'Texnologiya'},
    {'id': 4, 'name': 'Elnur'}
]

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            errored: false,
            incomes: [],
            expense: []
        }

    }

    addData = (event) => {
        event.preventDefault();

        const _target = event.target;

        let type = _target.type.value,
            amount = parseInt(_target.amount.value),
            note = _target.note.value,
            category = _target.category.value;

        let data = {
            _id: Math.random().toString(36).substr(2, 9),
            type,
            amount,
            note,
            category
        };

        if (!isNaN(data.amount)) {
            if (type === "+") {
                this.setState({
                    incomes: [...this.state.incomes, data],
                });
            } else {
                this.setState({
                    expense: [...this.state.expense, data],
                });
            }
            this.setState({
                errored: false
            });
        } else {
            this.setState({
                errored: true
            });
        }


    }

    render() {

        let incomes = this.state.incomes;
        let expense = this.state.expense;

        const data = {expense, incomes};

        let incomesSum = 0;
        for (let i = 0; i < incomes.length; i++) {
            incomesSum += incomes[i].amount;
        }

        let expenseSum = 0;
        for (let i = 0; i < expense.length; i++) {
            expenseSum += expense[i].amount;
        }

        return (
            <div className="App">
                <div className="top-side">
                    <h3 className="text-white text-center pt-3">Aprel ayı üçün büdcəniz</h3>
                    <div className="all_money text-center text-white">
                        {incomesSum - expenseSum} AZN
                    </div>
                    <div className="budget-total mt-3">
                        <Row>
                            <Col>
                                <Alert variant="primary">
                                    Gelir: {incomesSum} AZN
                                </Alert>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Alert variant="danger">
                                    Xerc: {expenseSum} AZN
                                </Alert>
                            </Col>
                        </Row>
                    </div>

                    <Addform categories={categories} errored={this.state.errored} addData={this.addData}/>
                    <div className="clearfix"></div>
                </div>

                <Budgettable categories={categories} data={data}/>

            </div>
        );
    }
}

export default App;
